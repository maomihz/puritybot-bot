import os
import logging
from . import MyClient
from .config import ChannelConfig
from argparse import ArgumentParser
from configparser import ConfigParser

# Configure logging
logger = logging.getLogger('discord')
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s')

handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
handler.setFormatter(formatter)
logger.addHandler(handler)

console = logging.StreamHandler()
console.setFormatter(formatter)
logger.addHandler(console)

parser = ArgumentParser()
parser.add_argument('config', default='config.ini',
                    help='Path of configuration file')

args = parser.parse_args()

configfile = ConfigParser()
configfile.read(args.config)
configs_section = configfile['configs']
commands_section = configfile['commands']

config = ChannelConfig()

config.channel_id = configs_section.getint('channel_id')
config.user_activity_wait = configs_section.getint('user_activity_wait')
config.send_activity_wait = configs_section.getint('send_activity_wait')

for cmd, cooldown in commands_section.items():
    cooldown = int(cooldown)
    if cooldown > 0:
        config.commands[cmd] = int(cooldown)

logger.info(config)

TOKEN = os.environ['TOKEN']
client = MyClient(config)
client.run(TOKEN, bot=False)
