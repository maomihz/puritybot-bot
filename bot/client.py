import re
import json
import asyncio
import logging
from time import time
from random import random

import discord
from discord.ext import tasks


class MyClient(discord.Client):
    def __init__(self, config):
        super().__init__()
        self.config = config
        self.commands = config.commands
        self.coins_regex = re.compile(config.coins_regex)

        self.start_time = time()
        self.command_record = {}

        self.bot_replied = 0
        self.user_activity = 0

        self.log = logging.getLogger('discord')

        try:
            self.command_record = self._load_command_record()
        except FileNotFoundError as e:
            print('OSError', e)
        except json.decoder.JSONDecodeError as e:
            print('JSONDecodeError', e)

        for c in config.commands:
            if c not in self.command_record:
                self.command_record[c] = 0
        self._save_command_record()

    @property
    def _command_record_file(self):
        return 'command_{}.json'.format(self.config.channel_id)

    def _load_command_record(self):
        with open(self._command_record_file) as f:
            return json.load(f)

    def _save_command_record(self):
        with open(self._command_record_file, 'w') as f:
            json.dump(self.command_record, f)

    @property
    def send_activity(self):
        return max(self.command_record.values())

    @property
    def target_channel(self):
        return self.get_channel(self.config.channel_id)

    def message_cooldowns(self):
        cooldowns = {}
        for cmd, cooldown in self.commands.items():
            time_diff = self.command_record[cmd] + cooldown - time()
            cooldowns[cmd] = time_diff
        return cooldowns

    @tasks.loop(seconds=6.0)
    async def send_commands(self):
        user_diff = self.user_activity + self.config.user_activity_wait - time()
        if user_diff > 0:
            self.log.info('User activity skip: %ds...', user_diff)
            return

        send_diff = self.send_activity + self.config.send_activity_wait - time()
        if self.bot_replied == 0 and send_diff > 0:
            self.log.info('Wait for bot reply: %ds...', send_diff)
            return

        cooldowns = self.message_cooldowns()

        for cmd, cooldown in cooldowns.items():
            if cooldown > 0:
                continue
            self.bot_replied = 0
            self.log.info('Send: %s', cmd)

            async with self.target_channel.typing():
                await asyncio.sleep(random() + 1)
            await self.target_channel.send(cmd)

            self.command_record[cmd] = time()
            self._save_command_record()
            break
        else:
            cmd, t = min(cooldowns.items(), key=lambda x: x[1])
            self.log.info('%s in %ds', cmd, t)

    async def on_ready(self):
        print('Logged on as', self.user)
        self.send_commands.start()

    async def on_message(self, message):
        # don't respond to ourselves
        if message.author == self.user:
            return

        # Exclude message from other channels
        if message.channel.id != self.config.channel_id:
            return

        # Record user activity
        if not message.author.bot:
            self.user_activity = time()
            return

        # Check for bot message
        if message.content:
            msg = message.content
        elif len(message.embeds) == 1:
            msg = message.embeds[0].description
        else:
            return

        # Check the message is only for us
        if self.user.mention not in msg:
            return

        coins_match = self.coins_regex.search(msg)
        if not coins_match:
            return

        coins_gain = int(coins_match.group(1))
        self.log.info('Coins +%s', coins_gain)
        self.bot_replied = time()

    # Useless for now
    async def on_typing(self, channel, user, when):
        if channel.id != self.config.channel_id:
            return
        if user == self.user:
            return

        # Record user activity
        if not user.bot:
            self.user_activity = time()
            return
