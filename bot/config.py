class ChannelConfig:
    channel_id = 0
    user_activity_wait = 60
    send_activity_wait = 60
    commands = {}
    coins_regex = r'([0-9]+)\s*<:tgc_cornercoin'
